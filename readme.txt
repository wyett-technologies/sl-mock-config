Read Me
=======

Read me file for 'sl-mock-config'.

'sl-mock-config' is a collection of configuration files to be used with 'mock'
to build and test packages for Scientific Linux.

Structure
=========

sl_06/ - SL 6 (i686 and x86_64) mock config files.
sl_07/ - SL 7 (x86_64) mock config files.
create_release_archive.sh - Shell script to create release '.tar.gz' archive.
readme.txt - Read me file.
sl-mock-config.spec - RPM SPEC file for package.
sl-mock-config.yaml - Package contribution file.

Build Release Archive
=====================

Before building the archive. Please ensure you have made any necessary changes
to 'sl-mock-config.spec' i.e. update the version number, as this is used in the
archive creation shell script.

To build the archive, in a terminal:

'cd' into the root folder that contains the 'create_release_archive.sh' shell
script.

Run 'sh create_release_archive.sh'.

The release archive will be created in the root folder where the
'create_release_archive.sh' shell script is located.

What To Do Next
===============

Move the created release archive and spec file into your build environment,
create an SRPM from them and then build.
