#!/bin/bash

#
# File: create_release_archive.sh
#
# Description: Create release archive using info from spec file and
#              required files/data.
#

# Get package name from spec file.
package_name=$(awk '/Name:/{print $NF}' sl-mock-config.spec)

# Get package version from spec file.
package_version=$(awk '/Version:/{print $NF}' sl-mock-config.spec)

# Create folder with 'package_name' and 'package_version'.
mkdir "$package_name"-"$package_version"

# Copy files into created archive folder.
cp -f sl_*/*.cfg "$package_name"-"$package_version"

# Create archive from created archive folder.
tar zcf "$package_name"-"$package_version".tar.gz "$package_name"-"$package_version"

# Delete created archive folder.
rm -rf "$package_name"-"$package_version"
