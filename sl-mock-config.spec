Name: sl-mock-config
Version: 1.0.2
Release: 1.sl
Summary: mock configuration files for Scientific Linux

Group: Applications/System
License: GPL
URL: https://bitbucket.org/philwyett-vendetta/sl-mock-config/
Source0: %{name}-%{version}.tar.gz

BuildArch: noarch
BuildRequires: coreutils
Requires: mock
Obsoletes: mock-sl6-config

%description
mock configuration files for Scientific Linux.

%prep
%setup -q -n %{name}-%{version}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/etc/mock/
cp * %{buildroot}/etc/mock/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
/etc/mock/*

%changelog
* Fri Aug 07 2015 Phil Wyett <philwyett@wyett-technologies.co.uk> 1.0.2-1
- Revise to make one sl tagged package rather than seperate el6/el7 packages.

* Sun Feb 08 2015 Phil Wyett <philwyett.vendetta@gmail.com> 1.0.1-1
- Disable ccache within SL mock config files. Resolves #2.

* Wed Feb 04 2015 Phil Wyett <philwyett.vendetta@gmail.com> 1.0.0-1
- Initial release.
